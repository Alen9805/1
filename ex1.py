if __name__=='__main__':

    print('Ciao.')
    print('Sono Alen.')

    avg = 5.4444
    name = 'Alen'

    # Fist method to write variables
    print('The average of the values is %.3f' %avg)
    print('My name is %s'%name)

    # Second method to write variables
    print(f'The average of the values is {avg}')
    print(f'My name is {name}')

    # Special classes
    pi=3.14169265
    print(f'{pi:.4}')
    print('%.3f' %pi)
    pass

if __name__=='__main__':
    # Take a number in input
    print('\nCode to check if a number is a multiple of 2 and 3\n')
    a = int(input('Write a number:'))
    
    # Check if a is a multiple of 2 and 3
    if a%2==0 and a%3==0:
        print(f'\n{a} is a multiple of 2 and 3\n')
    elif a%2==0:
        print(f'\n{a} is a multiple of 2\n')
    elif a%3==0:
        print(f'\n{a} is a multiple of 3\n')
    else:
        print(f'\n{a} is not a multiple of 2 and 3\n')
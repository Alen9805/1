#write a script that ask as input the data needed to fillthe dict defined below and print the dict as output
import json
if __name__=='__main__':

    personal_data={}
    personal_data['projectname'] = input('Write the project name: ')
    personal_data['company'] = input('Write the company name: ')
    personal_data['device_list'] = []

    device = {}
    device['deviceID'] = input('Write the device ID: ')
    device['deviceName'] = input('Write the device name: ')
    device['deviceType'] = input('Write the device type: ')

    personal_data['device_list'].append(device)

    print(personal_data)

    personal_data = json.dumps(personal_data,indent=2)
    print(personal_data)

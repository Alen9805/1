# Esercizio 5
if __name__ == '__main__':

    f=open('original.txt')
    filecontent=f.read()
    f.close()

    f=open('copy.txt','w')
    s='The content of the file is\n'
    f.write(s+filecontent)
    f.close()

    f=open('copy.txt')
    filecontent1=f.read()
    f.close()

    print('\noriginal.txt\n\n')
    print(f'{filecontent}\n\n')
    print('copy.txt\n\n')
    print(f'{filecontent1}\n')

    pass
#write 4 function to: add, subtract, multiply and divide two given numbers and print the result
def addition(n1,n2):
    n = n1+n2
    print(f'addition: {n}\n')
    return n

def subtraction(n1,n2):
    n = n1-n2
    print(f'subtraction: {n}\n')
    return n

def multiplication(n1,n2):
    n = n1*n2
    print(f'multiplication: {n}\n')
    return n

def division(n1,n2):
    if n2 == 0:
        print('Not possible to divide a number by 0\n')
    else:
        n = n1/n2
        print(f'division: {n} \n')

if __name__=='__main__':
    n1=int(input('Write the first number: '))
    n2=int(input('Write the second number: '))
    print()

    n=addition(n1,n2)
    n=subtraction(n1,n2)
    n=multiplication(n1,n2)
    n=division(n1,n2)
